ics-ans-role-smtp-proxy
===================

Ansible role to install smtp-proxy.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-smtp-proxy
```

License
-------

BSD 2-clause
